#!/bin/bash

# Checking minikube status and start if not already running.
if ! minikube status
then
    if ! minikube start
    then
        exit
    fi
fi

# Add helm repo, installing and exposing prometheus operator stack
echo -e "=========== Installing and Configuring Prometheus operator stack ==========="
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm install kube-prometheus-stack prometheus-community/kube-prometheus-stack

# Build counter web container
echo -e "=========== Dockerizing counter web ==========="
if ! minikube image build -t counter .
then
    exit
fi

# Deploy counter and expose
echo -e "=========== Deploying counter web ==========="
kubectl apply -f counter-deployment.yml
kubectl expose deployment counter --type=NodePort --target-port=80 --name=counter-ex

# Deploy mysql
echo -e "=========== Deploying mysql ==========="
kubectl apply -f mysql-deployment.yml

# Exposing prometheus to localhost
echo -e "=========== Exposing prometheus ==========="
kubectl expose service prometheus-operated --type=NodePort --target-port=9090 --name=prom-ex

# Exposing Grafana to localhost
echo -e "=========== Exposing Grafana ==========="
kubectl expose service kube-prometheus-stack-grafana --type=NodePort --target-port=3000 --name=grafana-ex