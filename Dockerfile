FROM nginx:alpine
WORKDIR /usr/share/nginx/html
RUN rm -rf ./*
RUN apk add busybox-extras
COPY ./* ./
ENV MY_NAME Prasanna
ENTRYPOINT ["nginx", "-g", "daemon off;"]