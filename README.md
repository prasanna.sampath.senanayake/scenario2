# scenario2

Assumptions
===========

1. Following software and packages Installed on the machine which runs the local cluster
    - virtualbox
    - virtualbox-dkms package
    - conntrack package
    - linux headers
    - Docker
    - minikube
    - Helm

2. Internet connection avaialable for downloading packages and images.

Steps to install cluster
========================

1. Download the package to local machine where local cluster is to be setup.
2. From the root folder execute deploy_local.sh script.
3. Wait for few minutes after the installation completes for the containers to properly startup.

Login to database
=================
DB access can be done either by creating another client container or by login into the pod itself.

Running a DB client:
kubectl run -it --rm --image=mysql:5.6 --restart=Never mysql-client -- mysql -h mysql -ppassword

Prometheus
==========
Invoke prometheus through,
>> minikube service prom-ex

Grafana
=======
Invoke Grafana through,
>> minikube service grafana-ex

NOTE : grafana login dredentials are admin/prom-operator

Counter app
===========
Invoke counter app through,
>> minikube service counter-ex

Troublshooting
==============

- If installation breaks half way through it is better to clean up the cluster by doing a minikube delete, before restarting.
- If helm chart repos are already installed on your machine, it will not be re-installed. A meesage (""prometheus-community" already exists with the same configuration, skipping") will appear on the console. No need to troubleshoot.